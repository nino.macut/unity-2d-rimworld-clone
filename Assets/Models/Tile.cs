using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Tile
{
    public enum TileType { Empty, Grass, Floor };

    TileType type = TileType.Empty;

    Action<Tile> callbackTileTypeChanged;

    public TileType Type
    {
        get
        {
            return type;
        }
        set
        {
            TileType oldType = type;
            type = value;
            //Call the callback and let things know the tile has changed.
            if (callbackTileTypeChanged != null && oldType != type)
            {
                callbackTileTypeChanged(this);
            }
        }
    }

    LooseObject looseObject;
    InstalledObject installedObject;

    Map map;

    int x;
    public int X
    {
        get
        {
            return x;
        }
    }

    int y;
    public int Y
    {
        get
        {
            return y;
        }
    }

    public Tile(Map map, int x, int y)
    {
        this.map = map;
        this.x = x;
        this.y = y;
    }

    public void RegisterTileTypeChangedCallback(Action<Tile> callback)
    {
        callbackTileTypeChanged += callback;
    }

    public void UnregisterTileTypeChangedCallback(Action<Tile> callback)
    {
        callbackTileTypeChanged -= callback;
    }
}
